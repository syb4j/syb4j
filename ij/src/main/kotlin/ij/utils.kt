/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

package ij

import com.intellij.openapi.project.Project

val Project.isSYB4JEnabled: Boolean
    get() = true //TODO