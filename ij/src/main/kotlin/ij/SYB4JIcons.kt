// Copyright (c) 2021-2021, syb4j. Use of this source code is governed by the BSD 3-Clause License that can be found in the LICENSE file.

package ij

import com.intellij.openapi.util.IconLoader
import javax.swing.Icon

object SYB4JIcons {
    private fun load(path: String): Icon {
        return IconLoader.getIcon(path, SYB4JIcons::class.java)
    }

    val LAMBDA: Icon = load("/icons/lambda.svg")
}