package ij.augment

import syb4j.types.Method
import syb4j.types.Method.Abstract
import syb4j.types.Method.Constructor
import syb4j.types.toJavaCode
import com.intellij.psi.JavaPsiFacade
import com.intellij.psi.PsiClass
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiMethod

internal fun PsiClass.toPsi(methods: List<Method>): List<PsiMethod> {
    val factory = JavaPsiFacade.getElementFactory(project)
    return methods.map { method ->
        factory.createMethodFromText(method.toJavaCode().toString().also { println(it) }, this)
            .let { psi ->
                @Suppress("UnstableApiUsage")
                object : PsiMethod by psi {
                    override fun getContainingClass(): PsiClass = this@toPsi
                    override fun getSourceElement(): PsiElement? = psi.sourceElement
                    override fun equals(other: Any?): Boolean = psi == other
                }
            }
    }

}


/*return methods.map { method ->
        LightMethodBuilder(manager, language, method.getIjName(ast.name ?: "<init>"))
            .apply {
                containingClass = ast
                navigationElement = ast
//                setBaseIcon(ADTIcons.LAMBDA)
                isConstructor = method is Method.Constructor
                addModifiers(method)
                val substitutor = method.typeParams.foldIndexed(PsiSubstitutor.EMPTY) { index, substitutor, rTVar ->
                    val tVar = LightTypeParameterBuilder(rTVar.name, this, index)
                    addTypeParameter(tVar)
                    substitutor.put(tVar, PsiSubstitutor.EMPTY.substitute(tVar))
                }
                method.params.forEach {
                    addParameter(it.name,
                        factory.createTypeFromText(it.type.toJavaString().toString(), this)
//                            .let { type -> substitutor.substitute(type) }
                    )
                }
                setMethodReturnType(factory.createTypeFromText(method.returnType.toJavaString().toString(), this)
//                    .let { type -> substitutor.substitute(type) }
                )
            }
    }

private fun LightMethodBuilder.addModifiers(method: Method): Unit {
    method.modifiers.forEach { addModifier(it.name.toLowerCase()) }
    if (method is Method.Normal && method.isStatic) addModifier(PsiModifier.STATIC)
    if (method is Method.Abstract) addModifier(PsiModifier.ABSTRACT)
}
    */
