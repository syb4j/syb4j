// Copyright (c) 2021-2021, syb4j. Use of this source code is governed by the BSD 3-Clause License that can be found in the LICENSE file.

package ij.augment

import syb4j.core.`@Case`
import syb4j.types.*
import syb4j.types.TypeRep.*
import com.intellij.psi.*
import com.intellij.psi.impl.source.PsiClassReferenceType
import com.intellij.refactoring.extractMethod.newImpl.ExtractMethodHelper.hasExplicitModifier

internal fun read(dataElement: PsiClass): SealedClass<PsiClass> {
    val typeVars = dataElement.typeParameters.map { readTypeVar(it) }
    return SealedClass(
        name = QualifiedName(dataElement.qualifiedName ?: "null_cls_name"),
        typeVars = typeVars,
        cases = dataElement.allInnerClasses
            .filter { it.hasAnnotation(`@Case`.name) }
            .map { readCase(it, typeVars) },
        astElement = dataElement
    )
}

private fun readCase(caseElement: PsiClass, dataTypeVars: List<RTypeVar>): CaseClass<PsiClass> {
    val typeVars = caseElement.typeParameters.map { readTypeVar(it) }
    val scope = (typeVars + dataTypeVars).associateBy { it.name }

    val extendsClause = caseElement.extendsList?.referencedTypes?.firstOrNull()
        ?.let { readType(it, scope) } as? RObj
    return CaseClass(
        name = QualifiedName(caseElement.qualifiedName.toString()),
        typeVars = typeVars,
        extendsClause,
        fields = caseElement.allFields
            .filterNot { it.hasExplicitModifier("static") }
            .map { readField(it, scope) },
        astElement = caseElement
    )
}

private fun readField(fieldElement: PsiField, scope: Map<String, RTypeVar>): Field =
    Field(fieldElement.name, readType(fieldElement.type, scope))


private fun readType(psi: PsiType, typeVarScope: Map<String, RTypeVar>): TypeRep =
    when (psi) {
        is PsiPrimitiveType -> RPrimitive(psi.name)
        is PsiClassReferenceType -> typeVarScope[psi.name] ?: RObj(
            QualifiedName(psi.reference.qualifiedName),
            psi.parameters.map { readType(it, typeVarScope) })
        else -> RObj(QualifiedName(psi.canonicalText), emptyList())
    }

private fun readTypeVar(typeVarElement: PsiTypeParameter): RTypeVar =
    RTypeVar(typeVarElement.name ?: "null_type_var")



