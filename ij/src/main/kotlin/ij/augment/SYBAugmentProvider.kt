// Copyright (c) 2021-2021, syb4j. Use of this source code is governed by the BSD 3-Clause License that can be found in the LICENSE file.

package ij.augment

import com.intellij.openapi.module.ModuleUtilCore
import com.intellij.openapi.roots.OrderEnumerator
import com.intellij.openapi.util.Key
import com.intellij.openapi.util.RecursionGuard
import com.intellij.openapi.util.RecursionManager
import com.intellij.openapi.util.io.FileUtil
import com.intellij.psi.*
import com.intellij.psi.augment.PsiAugmentProvider
import com.intellij.psi.util.*
import com.intellij.util.lang.UrlClassLoader
import syb4j.core.SybResult
import syb4j.core.`@Case`
import syb4j.core.`@Sealed`
import syb4j.core.process
import syb4j.generics.Derivator
import ij.isSYB4JEnabled
import java.io.File
import java.util.*
import kotlin.io.path.toPath


var i = 0
var j = 0

private val ADT_KEY = Key<CachedValue<SybResult<PsiClass>>>("adt")
private val METHOD_KEY = Key<CachedValue<List<PsiMethod>>>("gen-methods")

@Suppress("UNCHECKED_CAST")
class Provider : PsiAugmentProvider() {
    companion object {
        private val adtGuard = RecursionManager.createGuard<PsiElement>("adt.augment.adt")
        private val psiGuard = RecursionManager.createGuard<PsiElement>("adt.augment.psi")
    }

    private inline val PsiClass.isADTDeclaration: Boolean
        get() = hasAnnotation(`@Sealed`.name) && !isAnnotationType && !isInterface


    private inline val PsiClass.isCaseDeclaration: Boolean
        get() = hasAnnotation(`@Case`.name) && !isAnnotationType && !isInterface

    override fun <Psi : PsiElement> getAugments(element: PsiElement, type: Class<Psi>) =
        getAugments(element, type, null)

    private val PsiClass.adt: SybResult<PsiClass>
        get() = getCachedValue(ADT_KEY, adtGuard, PsiModificationTracker.MODIFICATION_COUNT) {
            val s = System.currentTimeMillis()
            process(read(this), this.getDerivators()).also {
                println("ADT: ${++i} | ${System.currentTimeMillis() - s} ms")
            }
        }

    override fun transformModifiers(modifierList: PsiModifierList, modifiers: Set<String>): Set<String> =
        if (modifierList.project.isSYB4JEnabled) {
            val parent = modifierList.parent
            val result = HashSet(modifiers)
            if (parent is PsiClass) {
                if (parent.isADTDeclaration) {
                    result.add(PsiModifier.ABSTRACT)
                } else if (parent.isCaseDeclaration) {
                    result.add(PsiModifier.STATIC)
                    result.add(PsiModifier.PUBLIC)
                }
            } else {
                val parentClass = PsiTreeUtil.getParentOfType(modifierList, PsiClass::class.java, true)
                if (parentClass?.isCaseDeclaration == true && parent is PsiField) {
                    result.add(PsiModifier.PUBLIC)
                    result.add(PsiModifier.FINAL)
                }
            }
            result
        } else {
            modifiers
        }

    override fun <Psi : PsiElement> getAugments(
        element: PsiElement, //Class in which it is searched
        type: Class<Psi>, //Type of element which is considered
        nameHint: String?,
    ): List<Psi> {
        return if (element is PsiClass
            && type == PsiMethod::class.java
            && element.project.isSYB4JEnabled
        ) {
//            println("$element | $type | $nameHint")
//            println("j: ${++j}")
            val adt =
                if (element.isADTDeclaration) {
                    element.adt
                } else if (element.isCaseDeclaration) {
                    val parentADT = element.parent as? PsiClass
                    if (parentADT?.isADTDeclaration == true) {
                        parentADT.adt
                    } else null
                } else null
            adt?.fold(
                { (_, error) ->
                    println(error)
                    emptyList()
                },
                {
//                    element.getCachedValue(METHOD_KEY, psiGuard, element) {
                    val s = System.currentTimeMillis()
                    val methods =
                        if (element.isADTDeclaration) it.methods
                        else it.constructors.find { con -> con.ast.qualifiedName == element.qualifiedName }!!.methods
                    element.toPsi(methods).also {
                        println("METHODS: ${++j} | ${System.currentTimeMillis() - s} ms")
                    }
//                    }
                }) as? List<Psi> ?: emptyList()
        } else {
            emptyList()
        }
    }

    private fun PsiClass.getDerivators(): List<Derivator> {
        val urls = OrderEnumerator
            .orderEntries(ModuleUtilCore.findModuleForPsiElement(this)!!)
            .recursively()
            .pathsList
            .pathList
            .mapNotNull { path ->
                kotlin.runCatching {
                    File(FileUtil.toSystemIndependentName(path)).toURI().toPath()
                }.getOrNull()
            }

        val loader = UrlClassLoader.build()
            .files(urls)
            .parent(Provider::class.java.classLoader)
            .get()
        val toList = ServiceLoader.load(Derivator::class.java, loader).toList()
        return toList
    }
}


private fun <T> PsiElement.getCachedValue(
    key: Key<CachedValue<T>>,
    guard: RecursionGuard<PsiElement>,
    dependency: Any,
    compute: () -> T,
): T =
    CachedValuesManager.getCachedValue(this, key) {
        guard.doPreventingRecursion(this, true) {
            CachedValueProvider.Result.create(compute(), dependency)
        }
    }


