package test;

import syb4j.Case;
import syb4j.Sealed;
import syb4j.generics.Derivator;

import java.util.ServiceLoader;
import java.util.function.Function;

@Sealed class Option<A> {

    // @formatter:off

    @Case static class None    {}
    @Case static class Some<A> { A value; }

    // @formatter:on

    //region Option helper methods
/*
    <B> Option<B> map(Function<A, B> transform) {
        return flatMap(value -> Some(transform.apply(value)));
    }

    <B> Option<B> flatMap(Function<A, Option<B>> transform) {
        return match(
                none -> None(),
                some -> transform.apply(some.value)
        );
    }

    Option<A> print() {
        System.out.println(this);
        return this;
    }

    */
/*@Override public String toString() {
        return match(
                __ -> "Nothing",
                some -> "Some(" + some.value + ")"
        );
    }*//*

    //endregion

    private static Option<Integer> isEven(Integer i) {
        return i % 2 == 0 ? Some(i) : None();
    }

    public static void main(String[] args) {
        for (Derivator derivator : ServiceLoader.load(Derivator.class)) {
            System.out.println(derivator.getClass());
        }
        Some(10).print()
                .flatMap(i -> isEven(i))
                .print()
                .map(i -> i * 2)
                .print();
    }
*/

}

