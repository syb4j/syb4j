package test;

public class EPOO {
    interface Exp<A> {
        A eval();
    }

    static class Lit<A> implements Exp<A> {
        A x;

        Lit(A x) {
            this.x = x;
        }

        public A eval() {
            return x;
        }
    }

    abstract static class Add implements Exp<Integer> {
        abstract Exp<Integer> getE1();

        abstract Exp<Integer> getE2();

        public Integer eval() {
            return getE1().eval() + getE2().eval();
        }
    }

    static class AddFinal extends Add {
        Exp<Integer> e1, e2;

        AddFinal(Exp<Integer> e1, Exp<Integer> e2) {
            this.e1 = e1;
            this.e2 = e2;
        }

        Exp<Integer> getE1() {
            return e1;
        }

        Exp<Integer> getE2() {
            return e2;
        }
    }

    interface Show<A> extends Exp<A> {
        String print();
    }

    static class LitP<A> extends Lit<A> implements Show<A> {

        LitP(A x) {
            super(x);
        }

        public String print() {
            return x.toString();
        }

    }

    abstract static class AddP extends Add implements Show<Integer> {
        abstract Show<Integer> getE1();

        abstract Show<Integer> getE2();

        public String print() {
            return "(" + getE1().print() + " + " + getE2().print() + ")";
        }

    }

    static class AddPFinal extends AddP {
        Show<Integer> e1, e2;

        AddPFinal(Show<Integer> e1, Show<Integer> e2) {
            this.e1 = e1;
            this.e2 = e2;
        }

        Show<Integer> getE1() {
            return e1;
        }

        Show<Integer> getE2() {
            return e2;
        }
    }

    static class BoolP implements Show<Boolean> {
        boolean b;

        public BoolP(boolean b) {
            this.b = b;
        }

        @Override public Boolean eval() {
            return b;
        }

        @Override public String print() {
            return "" + b;
        }
    }

    static class IfP<A> implements Show<A> {
        Show<Boolean> cond;
        Show<A> then, els;

        public IfP(Show<Boolean> cond, Show<A> then, Show<A> els) {
            this.cond = cond;
            this.then = then;
            this.els = els;
        }

        @Override public A eval() {
            return cond.eval() ? then.eval() : els.eval();
        }

        @Override public String print() {
            return "if " + cond.print() + " then " + then.print() + " else " + els.print();
        }
    }


    public static void main(String[] args) {
        var exp = new AddPFinal(new LitP<>(7), new LitP<>(4));
        var s = new IfP<>(new BoolP(false), exp, new LitP<>(0));
        System.out.println(s.eval());
        System.out.println(s.print());
    }
}
