package test;

import syb4j.Case;
import syb4j.Sealed;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

// (12 + 213) * 234
@Sealed class Expr<A> {

    // @formatter:off
    @Case static class Lit<A>                           { A value;                                 }
    @Case static class Add      extends Expr<Integer>   { Expr<Integer> left; Expr<Integer> right; }
    @Case static class Mul      extends Expr<Integer>   { Expr<Integer> left; Expr<Integer> right; }
    @Case static class Div      extends Expr<Integer>   { Expr<Integer> left; Expr<Integer> right; }
    @Case static class Eq<A>    extends Expr<Boolean>   { Expr<A> left; Expr<A> right;             }
    @Case static class Array<A> extends Expr<List<A>>   { List<Expr<A>> elements;                  }
    // @formatter:on


/*

    public static void main(String[] args) {
        var a = Lit(2);
        var b = Lit(3);
        var c = Lit(4);

        var add = Add(a, b); // 2 + 3
        var mul = Mul(c, add); // 4 * (2 + 3)
        var mul2 = Mul(mul, mul); // (4 * (2 + 3))²
        var eq = Eq(mul2, Lit(eval(mul) * eval(mul)));

        var arr = Array(asList(mul2, a, b, c, add));

        print(a);
        print(b);
        print(c);
        print(add);
        print(mul2);
        print(eq);
        print(arr);
    }

    private static void print(Expr<?> e) {
        System.out.println(prettyPrint(e) + " = " + eval(e));
    }

    public static String prettyPrint(Expr<?> e) {
        return e.match(
                lit -> lit.value.toString(),
                add -> '(' + prettyPrint(add.left) + " + " + prettyPrint(add.right) + ')',
                mul -> prettyPrint(mul.left) + " * " + prettyPrint(mul.right),
                div -> prettyPrint(div.left) + " / " + prettyPrint(div.right),
                eq -> prettyPrint(eq.left) + " == " + prettyPrint(eq.right),
                arr -> arr.elements.stream()
                        .map(Expr::prettyPrint)
                        .collect(Collectors
                                .joining(", ", "[", "]"))
        );
    }

    @SuppressWarnings("unchecked")
    public static <A> A eval(Expr<A> e) {
        return (A) e.match(
                lit -> lit.value,
                add -> eval(add.left) + eval(add.right),
                mul -> eval(mul.left) * eval(mul.right),
                div -> eval(div.left) / eval(div.right),
                eq -> eval(eq.left).equals(eval(eq.right)),
                arr -> arr.elements.stream()
                        .map(Expr::eval)
                        .collect(Collectors.toList())
        );
    }
*/

}

