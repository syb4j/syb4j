// Copyright (c) 2021, syb4j. Use of this source code is governed by the BSD 3-Clause License that can be found in the LICENSE file.

package test;

import java.util.function.BiFunction;
import java.util.function.Function;

abstract class Shape2 {
    abstract <R> R match(Function<Double, R> matchCircle,
                         BiFunction<Double, Double, R> matchRect);


    static Shape2 circle(double radius) {
        return new Shape2() {
            @Override <R> R match(Function<Double, R> matchCircle,
                                  BiFunction<Double, Double, R> matchRect) {
                return matchCircle.apply(radius);
            }
        };
    }

    static Shape2 rect(double x, double y) {
        return new Shape2() {
            @Override <R> R match(Function<Double, R> matchCircle,
                                  BiFunction<Double, Double, R> matchRect) {
                return matchRect.apply(x, y);
            }
        };
    }

    static double area(Shape2 shape) {
        return shape.match(
            r -> Math.PI * r,
            (x, y) -> x * y
        );
    }
}
