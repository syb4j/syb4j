package test;

import syb4j.Case;
import syb4j.Sealed;

import java.util.Scanner;
import java.util.function.Function;

@SuppressWarnings("Convert2MethodRef, unused")
@Sealed class Either<E, A> {
    // @formatter:off

    @Case static class Left <E, A> { E value; }
    @Case static class Right<E, A> { A value; }

    // @formatter:on

    //region Either helper methods
/*
    <B> Either<E, B> map(Function<A, B> transform) {
        return flatMap(value -> Right(transform.apply(value)));
    }

    <B> Either<E, B> flatMap(Function<A, Either<E, B>> transform) {
        return match(
                left -> Left(left.value),
                right -> transform.apply(right.value)
        );
    }
*/

    Either<E, A> print() {
        System.out.println(this);
        return this;
    }

  /*  @Override public String toString() {
        return match(
                left -> "Left(" + left.value + ")",
                right -> "Right(" + right.value + ")"
        );
    }*/
    //endregion

/*
    static Either<String, Integer> parseInt(String s) {
        if (s.matches("^[0-9]+$")) return Right(Integer.parseInt(s));
        else return Left(s + " is not a number");
    }

    static Either<String, Integer> validateAge(int age) {
        if (age < 18) return Left("Must be at least 18");
        else return Right(age);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s;
        while (!(s = in.nextLine()).equals("exit")) {
            parseInt(s)
                    .flatMap(i -> validateAge(i))
                    .map(age -> new User(age))
                    .print();
        }
    }
*/


    static class User {
        final int age;

        User(int age) {
            this.age = age;
        }

        @Override public String toString() {
            return "User{" +
                    "age=" + age +
                    '}';
        }
    }

}

