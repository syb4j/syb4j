package test;

public class ExprAlg {
    static <A> A mkExp(IntAlg<A> f) {
        return f.add(f.lit(1),
            f.add(f.lit(2), f.lit(3)));
    }

    static <A> A mkMulExp(MulAlg<A> f) {
        return f.mul(f.lit(10), mkExp(f));
    }

    public static void main(String[] args) {
        int six = mkExp(new IntEval()).eval();
        String result = mkExp(new IntPrint()); // (1 + (2 + 3))
        int sixty = mkMulExp(new MulEval()).eval();
        String mulExp = mkMulExp(new MulPrint());
    }
}

interface Eval {
    int eval();
}

interface IntAlg<A> {
    A lit(int x);
    A add(A e1, A e2);
}

class IntEval implements IntAlg<Eval> {
    public Eval lit(int x) {
        return () -> x;
    }

    public Eval add(Eval e1, Eval e2) {
        return () -> e1.eval() + e2.eval();
    }
}

interface MulAlg<A> extends IntAlg<A> {
    A mul(A e1, A e2);
}

class IntPrint implements IntAlg<String> { // statt IntAlg<Print>
    public String lit(final int x) {
        return Integer.toString(x);
    }

    public String add(final String e1, final String e2) {
        return " (" + e1 + " + " + e2 + " )";
    }
}

class MulEval extends IntEval implements MulAlg<Eval> {
    public Eval mul(Eval e1, Eval e2) {
        return () -> e1.eval() * e2.eval();
    }
}

class MulPrint extends IntPrint implements MulAlg<String> {
    public String mul(String e1, String e2) {
        return e1 + " * " + e2;
    }
}