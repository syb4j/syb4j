package test;

import syb4j.Case;
import syb4j.Sealed;

@Sealed public class Shape {
    @Case static class Circle { double r;    }
    @Case static class Rect   { double x, y; }

    static double area(Shape shape) {
        return shape.match(
            (Circle c) -> Math.PI * c.r * c.r,
            (Rect r) -> r.x * r.y
        );
    }
}