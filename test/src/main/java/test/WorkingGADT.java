package test;

import syb4j.TypeEq;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;

public abstract class WorkingGADT<A> {
    private WorkingGADT() {
    }

    public <B> List<WorkingGADT<B>> getAs(@NotNull TypeEq<WorkingGADT<List<B>>, WorkingGADT<A>> id) {
        Objects.requireNonNull(id);
        return ((Lis<B>) this).a;
    }

    public static <B> List<WorkingGADT<B>> getAs(@NotNull WorkingGADT<List<B>> gadt) {
        Objects.requireNonNull(gadt);
        return ((Lis<B>) gadt).a;
    }

    static <B> WorkingGADT<Boolean> Eq(WorkingGADT<B> a, WorkingGADT<B> b) {
        return new Eq<>(a, b, TypeEq.refl());
    }

    public static <A> WorkingGADT<List<A>> Lis(List<WorkingGADT<A>> as) {
        return new Lis<>(as);
    }

    static WorkingGADT<Integer> Lit(int i) {
        return new Lit(i, TypeEq.refl());
    }

    static WorkingGADT<Integer> Add(WorkingGADT<Integer> a, WorkingGADT<Integer> b) {
        return new Add(a, b, TypeEq.refl());
    }

    public static void main(String[] args) {
        WorkingGADT<Boolean> eq = Eq(Add(Lit(10), Lit(1)), Lit(11));
        WorkingGADT<List<Boolean>> lis = Lis(singletonList(eq));
        System.out.println(eval(lis));
        System.out.println(eval(Eq(lis, Lis(singletonList(Eq(Lit(0), Lit(0)))))));
        System.out.println(pretty(Eq(lis, Lis(singletonList(Eq(Lit(0), Lit(0)))))));

        List<Integer> eval = eval2(Lis(singletonList(Lit(1))));
        System.out.println("eval(a) = " + eval);
        int i = eval.get(0);

        System.out.println(lis.getAs(TypeEq.refl()));
        List<WorkingGADT<Boolean>> as = lis.getAs(TypeEq.refl());
        List<WorkingGADT<Boolean>> as2 = WorkingGADT.getAs(lis);
        System.out.println(!eval(as2.get(0)));
    }

    private static <T> T eval(WorkingGADT<T> e) {
        return e.match(
                (lit, id) -> id.coerce(lit.i),
                (add, id) -> id.coerce(eval(add.a) + eval(add.b)),
                (eq, id) -> id.coerce(eval(eq.a).equals(eval(eq.b))),
                new ForAllLis<T, T>() {
                    @Override public <B> T Lis(Lis<B> lis, TypeEq<List<B>, T> id) {
                        return id.coerce(lis.a.stream().map(WorkingGADT::eval).collect(Collectors.toList()));
                    }
                }
        );
    }

    private static <T> String pretty(WorkingGADT<T> e) {
        return e.<String>match(
                (lit, id) -> String.valueOf(lit.i),
                (add, id) -> pretty(add.a) + " + " + pretty(add.b),
                (eq, id) -> pretty(eq.a) + " = " + pretty(eq.b),
                new ForAllLis<>() {
                    @Override public <B> String Lis(Lis<B> lis, TypeEq<List<B>, T> id) {
                        return lis.a.stream().map(WorkingGADT::pretty).collect(Collectors.joining(", ", "[", "]"));
                    }
                }
        );
    }

    private static <T> T eval2(WorkingGADT<T> e) {
        return e.match(new Matcher<T, T>() {
            @Override public T Lit(Lit l, TypeEq<Integer, T> id) {
                return id.coerce(l.i);
            }

            @Override public T Add(Add add, TypeEq<Integer, T> id) {
                return id.coerce(eval2(add.a) + eval2(add.b));
            }

            @Override public <B> T Eq(Eq<B> eq, TypeEq<Boolean, T> id) {
                return id.coerce(eval2(eq.a).equals(eval(eq.b)));
            }

            @Override public <B> T Lis(Lis<B> lis, TypeEq<List<B>, T> id) {
                return id.coerce(lis.a.stream().map(WorkingGADT::eval2).collect(Collectors.toList()));
            }
        });
    }

    public abstract <R> R match(BiFunction<Lit, TypeEq<Integer, A>, R> Lit,
                                BiFunction<Add, TypeEq<Integer, A>, R> Add,
                                BiFunction<Eq<?>, TypeEq<Boolean, A>, R> Eq,
                                ForAllLis<R, A> Lis);

    public abstract <R> R match(Matcher<R, A> matcher);

    @FunctionalInterface
    interface ForAllLis<R, A> {
        <B> R Lis(Lis<B> lis, TypeEq<List<B>, A> id);
    }

    interface Matcher<R, A> {
        R Lit(Lit l, TypeEq<Integer, A> id);

        R Add(Add add, TypeEq<Integer, A> id);

        <B> R Eq(Eq<B> eq, TypeEq<Boolean, A> id);

        <B> R Lis(Lis<B> lis, TypeEq<List<B>, A> id);
    }


    private static final class Lis<B> extends WorkingGADT<List<B>> {
        final List<WorkingGADT<B>> a;
        final TypeEq<List<B>, List<B>> id = TypeEq.refl();


        private Lis(List<WorkingGADT<B>> a) {
            this.a = a;
        }

        @Override
        public <R> R match(BiFunction<Lit, TypeEq<Integer, List<B>>, R> Lit,
                           BiFunction<Add, TypeEq<Integer, List<B>>, R> Add,
                           BiFunction<Eq<?>, TypeEq<Boolean, List<B>>, R> Eq,
                           ForAllLis<R, List<B>> Lis) {
            return Lis.Lis(this, id);
        }

        @Override public <R> R match(Matcher<R, List<B>> matcher) {
            return matcher.Lis(this, TypeEq.refl());
        }
    }

    private static final class Eq<B> extends WorkingGADT<Boolean> {
        final WorkingGADT<B> a;
        final WorkingGADT<B> b;

        final TypeEq<Boolean, Boolean> id;


        private Eq(WorkingGADT<B> a, WorkingGADT<B> b, TypeEq<Boolean, Boolean> id) {
            this.a = a;
            this.b = b;
            this.id = id;
        }

        public <R> R match(BiFunction<Lit, TypeEq<Integer, Boolean>, R> Lit,
                           BiFunction<Add, TypeEq<Integer, Boolean>, R> Add,
                           BiFunction<Eq<?>, TypeEq<Boolean, Boolean>, R> Eq,
                           ForAllLis<R, Boolean> Lis) {
            return Eq.apply(this, id);
        }

        @Override public <R> R match(Matcher<R, Boolean> matcher) {
            return matcher.Eq(this, id);
        }
    }

    private static final class Add extends WorkingGADT<Integer> {

        final WorkingGADT<Integer> a;
        final WorkingGADT<Integer> b;
        final TypeEq<Integer, Integer> id;

        private Add(WorkingGADT<Integer> a, WorkingGADT<Integer> b, TypeEq<Integer, Integer> id) {
            this.a = a;
            this.b = b;
            this.id = id;
        }

        @Override
        public <R> R match(BiFunction<Lit, TypeEq<Integer, Integer>, R> Lit,
                           BiFunction<Add, TypeEq<Integer, Integer>, R> Add,
                           BiFunction<Eq<?>, TypeEq<Boolean, Integer>, R> Eq,
                           ForAllLis<R, Integer> Lis) {
            return Add.apply(this, id);
        }

        @Override public <R> R match(Matcher<R, Integer> matcher) {
            return matcher.Add(this, id);
        }
    }

    private static final class Lit extends WorkingGADT<Integer> {
        final int i;
        final TypeEq<Integer, Integer> id;

        private Lit(int i, TypeEq<Integer, Integer> id) {
            this.i = i;
            this.id = id;
        }

        @Override
        public <R> R match(BiFunction<Lit, TypeEq<Integer, Integer>, R> Lit,
                           BiFunction<Add, TypeEq<Integer, Integer>, R> Add,
                           BiFunction<Eq<?>, TypeEq<Boolean, Integer>, R> Eq,
                           ForAllLis<R, Integer> Lis) {
            return Lit.apply(this, id);
        }

        @Override public <R> R match(Matcher<R, Integer> matcher) {
            return matcher.Lit(this, id);
        }
    }
}
