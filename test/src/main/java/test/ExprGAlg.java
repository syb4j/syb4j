package test;

public class ExprGAlg {
    static <A> A mkExp(IntAlg<A> f) {
        return f.add(f.lit(1),
            f.add(f.lit(2), f.lit(3)));
    }

    static <A, B> A mkBoolExp(BoolAlg<A, B, A> f) {
        return f.iff(f.bool(true), f.lit(10), mkExp(f));
    }

    public static void main(String[] args) {
        System.out.println(mkExp(new IntEval()).eval());
        System.out.println(mkExp(new IntPrint()));
        System.out.println(mkBoolExp(new BoolEval<>()).eval());
        System.out.println(mkBoolExp(new BoolPrint()));
    }

    interface Eval<R> {
        R eval();
    }

    interface IntAlg<A> {
        A lit(int x);
        A add(A e1, A e2);
    }

    static class IntEval implements IntAlg<Eval<Integer>> {
        public Eval<Integer> lit(int x) {
            return () -> x;
        }

        public Eval<Integer> add(Eval<Integer> e1, Eval<Integer> e2) {
            return () -> e1.eval() + e2.eval();
        }
    }

    static class IntPrint implements IntAlg<String> { // statt IntAlg<Print>
        public String lit(final int x) {
            return Integer.toString(x);
        }

        public String add(final String e1, final String e2) {
            return "(" + e1 + " + " + e2 + ")";
        }
    }

    interface BoolAlg<A, B, C> extends IntAlg<C> {
        A iff(B cond, A e1, A e2);
        B bool(boolean b);
    }

    static class BoolEval<A> extends IntEval implements BoolAlg<Eval<A>, Eval<Boolean>, Eval<Integer>> {
        public Eval<A> iff(Eval<Boolean> cond, Eval<A> e1, Eval<A> e2) {
            return () -> cond.eval() ? e1.eval() : e2.eval();
        }

        public Eval<Boolean> bool(boolean b) {
            return () -> b;
        }
    }

    static class BoolPrint extends IntPrint implements BoolAlg<String, String, String> {
        public String iff(String cond, String e1, String e2) {
            return "if " + cond + " then " + e1 + " else " + e2;
        }

        public String bool(boolean b) {
            return b + "";
        }
    }
}
