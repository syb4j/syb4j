package test;

public class ShapeAlgDemo {
    static <A> A shape(ShapeAlg<A> v) {
        return v.circle(10);
    }

    public static void main(String[] args) {

        System.out.println("area = " + shape(new ShapeFactory()).area());
        System.out.println("circumference = " + shape(new ShapeCirc()).circumference());
    }
    interface Shape {
        double area();
    }

    interface ShapeAlg<A> {
        A circle(double r);

        A rect(double x, double y);
    }

    static class ShapeFactory implements ShapeAlg<Shape> {
        @Override public Shape circle(double r) {
            return new Circle(r);
        }

        @Override public Shape rect(double x, double y) {
            return new Rect(x, y);
        }
    }

    static class Rect implements Shape {

        final double x;
        final double y;

        public Rect(double x, double y) {
            this.x = x;
            this.y = y;
        }

        @Override public double area() {
            return x * y;
        }
    }

    static class Circle implements Shape {

        private final double r;

        public Circle(double r) {
            this.r = r;
        }

        @Override public double area() {
            return Math.PI * r * r;
        }
    }

    interface Show {
        double circumference();
    }

    static class ShapeCirc implements ShapeAlg<Show> {
        @Override public Show circle(double r) {
            return () -> 2 * Math.PI * r;
        }

        @Override public Show rect(double x, double y) {
            return () -> 2 * x + 2 * y;
        }
    }

}

