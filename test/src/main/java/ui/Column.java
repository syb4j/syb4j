// Copyright (c) 2021, syb4j. Use of this source code is governed by the BSD 3-Clause License that can be found in the LICENSE file.

package ui;

import lombok.Value;
import lombok.experimental.UtilityClass;
import lombok.val;
import org.intellij.lang.annotations.Language;
import syb4j.Case;
import syb4j.Sealed;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

import static java.util.Calendar.*;

// @formatter:off

@Value
class Table<T> { List<Column<T>> columns; }

@Sealed public class Column<T> {
    @Case
    static class Text<T>     { String label; Function<T, String> getter; }
    @Case
    static class DateTime<T> { String label; Function<T, Date> getter; }
    @Case
    static class Num<T>      { String label; Function<T, Integer> getter; }

    public String getLabel() {
        return match(x -> x.label, x -> x.label, x -> x.label);
    }
}


@SuppressWarnings("deprecation")
@Value class Person {
    String firstName, lastName;
    Date dateOfBirth;
    int getAge() {
        return new Date().getYear() - dateOfBirth.getYear();
    }
}


@UtilityClass class HTMLTable {
    static Table<Person> PERSON_TABLE = new Table<>(
      List.of(
          Column.Text    ("Vorname",    Person::getFirstName),
          Column.Text    ("Nachname",   Person::getLastName),
          Column.Num     ("Alter",      Person::getAge),
          Column.DateTime("Geburtstag", Person::getDateOfBirth)
      )
    );
    // @formatter:on

    static List<Person> PERSONS = List.of(
        new Person("Bob", "Smith", new Date(100, FEBRUARY, 20)),
        new Person("Max", "Musterman", new Date(65, DECEMBER, 1)),
        new Person("Alice", "Scott", new Date(30, JULY, 31))
    );

    @Language("HTML")
    public static final String STYLE = "<style>" +
        "table, th, td {border: 1px solid black;}" +
        "</style>";

    static <T> String displayTable(Table<T> table, List<T> rows) {
        val tableHtml = table.getColumns().stream()
            .reduce(new StringBuilder(STYLE + "<table><tr>"),
                (html, column) -> html.append("<th>")
                    .append(column.getLabel())
                    .append("</th>"),
                StringBuilder::append)
            .append("</tr>");
        rows.forEach(row -> {
            StringBuilder rowHtml = table.getColumns().stream()
                .reduce(new StringBuilder("<tr>"),
                    (html, column) -> html.append("<td>")
                        .append(formatCell(column, row))
                        .append("</td>"),
                    StringBuilder::append)
                .append("</tr>");
            tableHtml.append(rowHtml);
        });
        return tableHtml.append("</table>").toString();
    }

    private static <T> String formatCell(Column<T> column, T row) {
        return column.match(
            text -> text.getter.apply(row),
            date -> date.getter.apply(row).toLocaleString(),
            num -> num.getter.apply(row) + ""
        );
    }

    public static void main(String[] args) throws IOException {
        final FileWriter fileWriter = new FileWriter("table.html");
        fileWriter.write(displayTable(PERSON_TABLE, PERSONS));
        fileWriter.close();

        Runtime rt = Runtime.getRuntime();
        rt.exec("\"C:\\Program Files\\BraveSoftware\\Brave-Browser\\Application\\brave.exe\" " + new File("table.html").getAbsolutePath());
    }
}
