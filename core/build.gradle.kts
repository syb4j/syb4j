import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.20"
    kotlin("kapt") version "1.5.20"
    id("com.github.johnrengelman.shadow") version "6.1.0"
}

version = "1.0-SNAPSHOT"

val arrow_version = "1.0.0-SNAPSHOT" //0.13.2

repositories {
    maven { url = uri("https://oss.sonatype.org/content/repositories/snapshots/") }
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.1")

    implementation("io.arrow-kt:arrow-fx-coroutines:$arrow_version")
    implementation("io.arrow-kt:arrow-optics:$arrow_version")
    kapt("io.arrow-kt:arrow-meta:$arrow_version")
    testImplementation(kotlin("test"))
    testImplementation("org.junit.jupiter:junit-jupiter:5.7.0")

//    compileOnly(files("../adt-derivators/build/libs/adt-derivators.jar"))

//    testImplementation("org.eclipse.jdt.core.compiler:ecj:4.6.1")
//    implementation("eclipse:ejc:3.0M8")
//    testImplementation("org.eclipse.jdt:ecj:3.26.0")

//    testImplementation("org.jooq:joor:0.9.13")
//    testImplementation("org.jooq:joor-java-8:0.9.13")

//    testImplementation("com.google.testing.compile:compile-testing:0.19")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.withType<KotlinCompile>().all {
    sourceCompatibility = "11"
    targetCompatibility = "11"
    kotlinOptions {
        jvmTarget = "11"
        freeCompilerArgs = listOf("-Xjvm-default=all")
    }
}

tasks.withType<JavaCompile>().all {
    sourceCompatibility = "11"
    targetCompatibility = "11"
}

tasks.test {
    useJUnitPlatform()
}