package syb4j

object ExprAlg {
    fun <A> IntAlg<A>.mkExp(): A {
        return add(lit(1),
            add(lit(2), lit(3)))
    }

    private fun <A> MulAlg<A>.mkMulExp(): A {
        return mul(lit(10), mkExp())
    }

    @JvmStatic fun main(args: Array<String>) {
        val six = IntEval().mkExp().eval()
        val result = IntPrint().mkExp() // (1 + (2 + 3))
        val sixty = MulEval().mkMulExp().eval()
        val mulExp = MulPrint().mkMulExp()
    }

    fun <A> IntAlg<A>.mkExp2(): A =
        add(lit(1),
            add(lit(2), lit(3)))

}

internal interface Eval {
    fun eval(): Int
}

interface IntAlg<A> {
    fun lit(x: Int): A
    fun add(e1: A, e2: A): A
}

internal open class IntEval : IntAlg<Eval> {
    override fun lit(x: Int): Eval {
        return object : Eval {
            override fun eval(): Int {
                return x
            }
        }
    }

    override fun add(e1: Eval, e2: Eval): Eval {
        return object : Eval {
            override fun eval(): Int {
                return e1.eval() + e2.eval()
            }
        }
    }
}

interface MulAlg<A> : IntAlg<A> {
    fun mul(e1: A, e2: A): A
}

internal open class IntPrint : IntAlg<String> {
    // statt IntAlg<Print>
    override fun lit(x: Int): String {
        return Integer.toString(x)
    }

    override fun add(e1: String, e2: String): String {
        return " ($e1 + $e2 )"
    }
}

internal class MulEval : IntEval(), MulAlg<Eval> {
    override fun mul(e1: Eval, e2: Eval): Eval {
        return object : Eval {
            override fun eval(): Int {
                return e1.eval() * e2.eval()
            }
        }
    }
}

internal class MulPrint : IntPrint(), MulAlg<String> {
    override fun mul(e1: String, e2: String): String {
        return "$e1 * $e2"
    }
}