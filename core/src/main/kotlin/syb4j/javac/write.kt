@file:Suppress("JAVA_MODULE_DOES_NOT_EXPORT_PACKAGE", "ObjectPropertyName", "RedundantSuspendModifier")

package syb4j.javac

import syb4j.core.`@Case`
import syb4j.core.`@Sealed`
import syb4j.types.*
import syb4j.types.TypeRep.RPrimitive.Type
import com.sun.source.util.Trees
import com.sun.tools.javac.code.BoundKind
import com.sun.tools.javac.code.Flags.*
import com.sun.tools.javac.code.Symtab
import com.sun.tools.javac.code.TypeTag
import com.sun.tools.javac.tree.JCTree
import com.sun.tools.javac.tree.JCTree.*
import com.sun.tools.javac.tree.TreeMaker
import com.sun.tools.javac.util.List
import com.sun.tools.javac.util.Name
import javax.lang.model.element.TypeElement
import com.sun.tools.javac.code.Type as JCType

internal fun write(
    trees: Trees,
    make: TreeMaker,
    names: Name.Table,
    symtab: Symtab,
): (ADTOut<TypeElement>) -> Unit = { syb4j ->
    val dataTree = trees.getTree(syb4j.ast) as JCClassDecl
    val cases = syb4j.constructors.map { case -> case to trees.getTree(case.ast) as JCClassDecl }

    make.at(dataTree.pos)

    dataTree.apply {
        removeAnnotations(`@Sealed`)
        setMods(PUBLIC or ABSTRACT)
        addDefs(syb4j.methods.map { it.toJCTree(make, names, symtab) })
    }

    cases.forEach { (caseADT, ast) ->
        ast.removeAnnotations(`@Case`)
        ast.setMods(PUBLIC or STATIC or FINAL)
        ast.typarams = caseADT.constructor.typeVars.map { it.toJCTypeParam(make, names) }.toJCList()
        ast.extending = syb4j.adt.typeRep(caseADT.constructor.boundTypeVars)
            .toJCExpr(make, names, symtab)

        ast.defs.forEach { if (it is JCVariableDecl) it.mods.flags = it.mods.flags or (FINAL or PUBLIC).toLong() }
        ast.addDefs(caseADT.methods.map { it.toJCTree(make, names, symtab) })
    }
}

private fun JCClassDecl.removeAnnotations(qualifiedName: QualifiedName) {
    mods.annotations = mods.annotations.filterNot { QualifiedName(it.type.toString()) == qualifiedName }
        .toJCList()
}


private fun JCClassDecl.setMods(flags: Int): Unit {
    mods.flags = flags.toLong()
}

private fun JCClassDecl.addDefs(defs: kotlin.collections.List<JCTree>): Unit {
    this.defs = (this.defs + defs).toJCList()
}

private val Method.jcName: String
    get() = when (this) {
        is Method.Constructor -> "<init>"
        is Method.Abstract -> name
        is Method.Normal -> name
    }
private val Modifier.flags: Int
    get() = when (this) {
        Modifier.Public -> PUBLIC
        Modifier.Final -> FINAL
        Modifier.Private -> PRIVATE
    }

private fun <T> Iterable<T>.toJCList(): List<T> = List.from(this)

//region JC* Translations


private fun Method.toJCTree(make: TreeMaker, names: Name.Table, symtab: Symtab): JCMethodDecl {
    println(toJavaCode())
    return make.MethodDef(
        make.Modifiers(jCFlags, getAnnotations(make, names)),
        names.fromString(jcName),
        returnType.toJCExpr(make, names, symtab),
        typeParams.map { it.toJCTypeParam(make, names) }.toJCList(),
        params.map { it.toJCParam(make, names, symtab) }.toJCList(),
        List.nil(),
        code?.toJCTree(make, names, symtab) as? JCBlock,
        null
    )
}

private fun TypeRep.RTypeVar.toJCTypeParam(make: TreeMaker, names: Name.Table): JCTypeParameter =
    make.TypeParameter(names.fromString(name), List.nil())

private fun Field.toJCParam(make: TreeMaker, names: Name.Table, symtab: Symtab): JCVariableDecl =
    make.VarDef(
        make.Modifiers(PARAMETER),
        names.fromString(name),
        type.toJCExpr(make, names, symtab),
        null
    )

private fun Stmt.toJCTree(make: TreeMaker, names: Name.Table, symtab: Symtab): JCStatement = when (this) {
    is Stmt.Assign -> make.Exec(make.Assign(lhs.toJCExpr(make, names, symtab), rhs.toJCExpr(make, names, symtab)))
    is Stmt.Block -> make.Block(0, stmts.map { it.toJCTree(make, names, symtab) }.toJCList())
    is Stmt.Decl -> TODO()
    is Stmt.If -> TODO()
    is Stmt.While -> TODO()
    is Stmt.Wrap -> make.Exec(expr.toJCExpr(make, names, symtab))
    is Stmt.Return -> make.Return(expr?.toJCExpr(make, names, symtab))
}

private fun Expr.toJCExpr(make: TreeMaker, names: Name.Table, symtab: Symtab): JCExpression = when (this) {
    Expr.Null -> make.Literal(TypeTag.BOT, null)
    Expr.This -> make.Ident(names.fromString("this"))
    is Expr.Var -> make.Ident(names.fromString(name))
    is Expr.Access -> make.Select(lhs.toJCExpr(make, names, symtab), names.fromString(rhs))
    is Expr.Binary -> TODO()
    is Expr.Literal -> TODO()
    is Expr.Unary -> TODO()
    is Expr.MethodCall -> make.Apply(
        List.nil(),
        lhs.toJCExpr(make, names, symtab),
        args.map { it.toJCExpr(make, names, symtab) }.toJCList()
    )
    is Expr.ConstructorCall -> make.NewClass(
        null,
        List.nil(),
        if (typeArgs) make.TypeApply(lhs.toJCExpr(make, names, symtab), List.nil())
        else lhs.toJCExpr(make, names, symtab),
        args.map { it.toJCExpr(make, names, symtab) }.toJCList(),
        null
    )
}

private fun TypeRep.toJCExpr(
    make: TreeMaker,
    names: Name.Table,
    symtab: Symtab,
): JCExpression = when (this) {
    is TypeRep.RWildcard -> make.Wildcard(
        make.TypeBoundKind(BoundKind.UNBOUND),
        make.Type(JCType.WildcardType(symtab.objectType, BoundKind.UNBOUND, symtab.boundClass))
    )
    is TypeRep.RTypeVar -> make.Ident(names.fromString(name))
    is TypeRep.RObj -> {
        val rawJCType = name.name.toJCExpr(make, names)
        if (typeVars.isEmpty()) {
            rawJCType
        } else {
            make.TypeApply(
                rawJCType,
                typeVars.map { it.toJCExpr(make, names, symtab) }.toJCList()
            )
        }
    }
    is TypeRep.RPrimitive -> when (type) {
        Type.Int -> make.TypeIdent(TypeTag.INT)
        Type.Bool -> make.TypeIdent(TypeTag.BOOLEAN)
        Type.Char -> make.TypeIdent(TypeTag.CHAR)
        Type.Float -> make.TypeIdent(TypeTag.FLOAT)
        Type.Double -> make.TypeIdent(TypeTag.DOUBLE)
        Type.Byte -> make.TypeIdent(TypeTag.BYTE)
        Type.Unit -> make.TypeIdent(TypeTag.VOID)
    }
}

private fun String.toJCExpr(make: TreeMaker, names: Name.Table): JCExpression =
    if (contains('.')) {
        val elements = split('.')
        make.Select(
            elements.subList(0, elements.size - 1).joinToString(".").toJCExpr(make, names),
            names.fromString(elements.last())
        )
    } else {
        make.Ident(names.fromString(this))
    }

private val Method.jCFlags: Long
    get() {
        val flags = modifiers.fold(0) { acc, modifier -> acc or modifier.flags }
        val static = if (this is Method.Normal && isStatic) STATIC else 0
        val abstract = if (this is Method.Abstract) ABSTRACT else 0
        return (flags or static or abstract).toLong()
    }

private fun Method.getAnnotations(make: TreeMaker, names: Name.Table): List<JCAnnotation> =
    if (this is Method.Normal)
        annotations.map { make.Annotation(it.name.toJCExpr(make, names), List.nil()) }.toJCList()
    else List.nil()


//endregion




