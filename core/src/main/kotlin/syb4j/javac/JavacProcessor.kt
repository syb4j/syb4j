/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

@file:Suppress("JAVA_MODULE_DOES_NOT_EXPORT_PACKAGE")

package syb4j.javac


import arrow.core.memoize
import com.sun.source.util.Trees
import com.sun.tools.javac.code.Symtab
import com.sun.tools.javac.code.Types
import com.sun.tools.javac.processing.JavacProcessingEnvironment
import com.sun.tools.javac.tree.TreeMaker
import com.sun.tools.javac.util.Name
import com.sun.tools.javac.util.Names
import kotlinx.coroutines.runBlocking
import syb4j.CoreBundle.message
import syb4j.Sealed
import syb4j.core.process
import syb4j.generics.Derivator
import syb4j.types.QualifiedName
import java.util.*
import javax.annotation.processing.*
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.TypeElement
import javax.tools.Diagnostic

@SupportedAnnotationTypes("syb4j.Sealed")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
class JavacProcessor : AbstractProcessor() {

    private lateinit var trees: Trees
    private lateinit var make: TreeMaker
    private lateinit var names: Name.Table
    private lateinit var symtab: Symtab
    private lateinit var types: Types
    private var disabled: Boolean = false

    private val derivators: () -> List<Derivator> = {
        ServiceLoader.load(Derivator::class.java, Derivator::class.java.classLoader).toList()
    }.memoize()

    override fun init(processingEnv: ProcessingEnvironment) {
        super.init(processingEnv)

        val context = (processingEnv as? JavacProcessingEnvironment)?.context

        if (context == null) {
            processingEnv.messager.printMessage(
                Diagnostic.Kind.ERROR,
                message("javac.not-javac", message("name"), processingEnv.javaClass.canonicalName)
            )
            disabled = true
        } else {
            trees = Trees.instance(processingEnv)
            make = TreeMaker.instance(context)
            names = Names.instance(context).table
            symtab = Symtab.instance(context)
            types = Types.instance(context)
        }
    }

    override fun process(annotations: MutableSet<out TypeElement>?, roundEnv: RoundEnvironment): Boolean {
        if (!disabled && !roundEnv.processingOver())
            runBlocking {
                roundEnv.getElementsAnnotatedWith(Sealed::class.java)
                    .filterIsInstance<TypeElement>()
                    .map(::read)
                    .map { process(it, derivators()) }
                    .forEach { result ->
                        result.fold(
                            { (elem, error) -> printErr(error.pretty, elem) },
                            write(trees, make, names, symtab)
                        )
                    }
            }
        return false
    }


    private fun printErr(error: String, element: Element) {
        processingEnv.messager.printMessage(Diagnostic.Kind.ERROR, error, element)
    }

}
