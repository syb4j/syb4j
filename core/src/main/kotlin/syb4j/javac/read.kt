/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

package syb4j.javac

import syb4j.Case
import syb4j.types.*
import syb4j.types.TypeRep.*
import javax.lang.model.element.TypeElement
import javax.lang.model.element.TypeParameterElement
import javax.lang.model.element.VariableElement
import javax.lang.model.type.DeclaredType
import javax.lang.model.type.PrimitiveType
import javax.lang.model.type.TypeMirror
import javax.lang.model.type.TypeVariable


internal fun read(dataElement: TypeElement): SealedClass<TypeElement> =
    SealedClass(
        name = QualifiedName(dataElement.qualifiedName),
        typeVars = dataElement.typeParameters.map { readTypeVar(it) },
        cases = dataElement.enclosedElements
            .filter { it.getAnnotation(Case::class.java) != null }
            .filterIsInstance<TypeElement>()
            .map { readCase(it) },
        astElement = dataElement
    )

private fun readCase(caseElement: TypeElement): CaseClass<TypeElement> {
    val extendsClause: RObj? =
        (caseElement.superclass as? DeclaredType)
            ?.let {
                RObj(it.qualifiedName, it.typeArguments.map { arg -> readType(arg) })
            }
    return CaseClass(
        name = QualifiedName(caseElement.qualifiedName.toString()),
        typeVars = caseElement.typeParameters.map { readTypeVar(it) },
        extendsClause,
        fields = caseElement.enclosedElements
            .filterIsInstance<VariableElement>()
            .map { readField(it) },
        astElement = caseElement
    )
}

private fun readField(fieldElement: VariableElement): Field =
    Field(fieldElement.simpleName.toString(), readType(fieldElement.asType()))


private fun readType(typeMirror: TypeMirror): TypeRep =
    when (typeMirror) {
        is TypeVariable -> RTypeVar(typeMirror.asElement().simpleName.toString())
        is PrimitiveType -> RPrimitive(typeMirror.toString())
        is DeclaredType ->
            RObj(typeMirror.qualifiedName, typeMirror.typeArguments.map { readType(it) })
        else -> RObj(QualifiedName(typeMirror.toString()), emptyList())
    }

private fun readTypeVar(typeVarElement: TypeParameterElement): RTypeVar =
    RTypeVar(typeVarElement.simpleName.toString())

private val DeclaredType.qualifiedName: QualifiedName
    get() = QualifiedName(toString().substringBefore('<'))



