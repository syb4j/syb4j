/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

package syb4j.types

import syb4j.types.TypeRep.RTypeVar


/**
 * (G)ADT definition
 * */
/*@optics()*/ data class ADT(
    val name: QualifiedName,
    val typeVars: List<RTypeVar>,
    val constructors: List<Constructor>
) {
    companion object

    fun typeRep(boundTypeVars: Map<String, TypeRep>): TypeRep =
        TypeRep.RObj(name, typeVars.map { boundTypeVars[it.name] ?: it })
}


/**
 * (G)ADT constructor
 *
 * @property boundTypeVars Only used for GADTs
 * */
/*@optics*/ data class Constructor(
    val name: QualifiedName,
    val typeVars: List<RTypeVar>,
    val boundTypeVars: Map<String, TypeRep> = emptyMap(),
    val fields: List<Field>
) {
    val typeRepFromParent: TypeRep
        get() {
            val reverseBinding = boundTypeVars.entries
                .associate { (from, to) ->
                    if (to is RTypeVar) to.name to RTypeVar(from)
                    else from to RTypeVar(from)
                }
            return TypeRep.RObj(name, typeVars.map {
                if (it.isExistential) TypeRep.RWildcard
                else reverseBinding[it.name] ?: it

            })
        }

    fun typeRepFromCase(constructor: Constructor): TypeRep {
        return TypeRep.RObj(name, typeVars.map {
            if (it.isExistential) TypeRep.RWildcard
            else constructor.boundTypeVars[it.name] ?: it
        })
    }

    companion object
}


