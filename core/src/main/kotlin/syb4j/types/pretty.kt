package syb4j.types


fun Method.toJavaCode(): StringBuilder = StringBuilder().also { sb ->
    addModifiers(sb)
    typeParams.joinTo(sb, ", ", " <", "> ") { it.name }

    if (this is Method.Normal || this is Method.Abstract)
        sb.append(returnType.toJavaString()).append(' ')

    sb.append(name)

    params.joinTo(sb, ", ", "(", ")") {
        it.type.toJavaString().append(' ').append(it.name)
    }

    sb.append(code?.toJavaCode() ?: ";")

}

private fun Stmt.toJavaCode(): StringBuilder = when (this) {
    is Stmt.Assign -> lhs.toJavaCode().append(" = ").append(rhs.toJavaCode()).append(';')
    is Stmt.Block -> stmts.joinTo(StringBuilder(), "\n", "{", "}") { it.toJavaCode() }
    is Stmt.Return -> StringBuilder("return ")
        .append(expr?.toJavaCode() ?: StringBuilder())
        .append(';')
    is Stmt.Wrap -> expr.toJavaCode().append(';')
    is Stmt.Decl -> TODO()
    is Stmt.If -> TODO()
    is Stmt.While -> TODO()
}

private fun Expr.toJavaCode(): StringBuilder = when (this) {
    Expr.Null -> StringBuilder("null")
    Expr.This -> StringBuilder("this")
    is Expr.Var -> StringBuilder(name)
    is Expr.Literal -> StringBuilder(value.toString())
    is Expr.Access -> lhs.toJavaCode().append('.').append(rhs)
    is Expr.Unary -> TODO()
    is Expr.Binary -> TODO()
    is Expr.ConstructorCall ->
        StringBuilder("new ").append(lhs.toJavaCode())
            .append(if (typeArgs) "<>" else "")
            .also { sb -> args.joinTo(sb, ", ", "(", ")") { it.toJavaCode() } }
    is Expr.MethodCall ->
        lhs.toJavaCode()
            .also { sb -> args.joinTo(sb, ", ", "(", ")") { it.toJavaCode() } }
}

private fun Method.addModifiers(sb: StringBuilder) {
    modifiers.forEach { sb.append(it.name.lowercase()).append(' ') }
    if (this is Method.Normal && isStatic) sb.append("static ")
    if (this is Method.Abstract) sb.append("abstract ")
}