/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

@file:Suppress("FunctionName", "EnumEntryName")

package syb4j.types

import syb4j.types.Expr.This
import syb4j.types.Expr.Var
import syb4j.types.Stmt.Assign
import syb4j.types.Stmt.Block
import syb4j.types.TypeRep.RPrimitive.Type.Unit


data class ADTOut<AST>(
    val ast: AST,
    val adt: ADT,
    val constructors: List<ConstructorOut<AST>>,
    val methods: List<Method>
) {
    companion object
}

data class ConstructorOut<AST>(
    val ast: AST,
    val constructor: Constructor,
    val methods: List<Method>
) {
    companion object
}

sealed class Method {
    abstract val modifiers: Set<Modifier>
    abstract val typeParams: List<TypeRep.RTypeVar>
    abstract val name: String
    abstract val params: List<Field>
    abstract val returnType: TypeRep
    abstract val code: Block?

    data class Abstract(
        override val name: String,
        override val modifiers: Set<Modifier> = emptySet(),
        override val typeParams: List<TypeRep.RTypeVar> = emptyList(),
        override val params: List<Field> = emptyList(),
        override val returnType: TypeRep,
    ) : Method() {
        override val code: Block? = null
    }

    data class Normal(
        val annotations: List<QualifiedName> = emptyList(),
        val isStatic: Boolean,
        override val name: String,
        override val modifiers: Set<Modifier> = emptySet(),
        override val typeParams: List<TypeRep.RTypeVar> = emptyList(),
        override val params: List<Field> = emptyList(),
        override val returnType: TypeRep,
        override val code: Block
    ) : Method()


    data class Constructor(
        override val modifiers: Set<Modifier> = emptySet(),
        override val typeParams: List<TypeRep.RTypeVar> = emptyList(),
        override val name: String,
        override val params: List<Field> = emptyList(),
        val assignFields: Boolean = false
    ) : Method() {
        override val returnType = TypeRep.RPrimitive(Unit)

        override val code: Block
            get() = if (assignFields)
                Block(params.map { This[it.name] `=` Var(it.name) })
            else Block(emptyList())

    }
}

sealed class Stmt {
    data class Block(val stmts: List<Stmt>) : Stmt()
    data class Assign(val lhs: Expr, val rhs: Expr) : Stmt()
    data class Decl(val type: TypeRep, val name: String, val rhs: Expr?) : Stmt()
    data class If(val condition: Expr, val trueCase: Stmt, val falseCase: Stmt?) : Stmt()
    data class While(val condition: Expr, val stmt: Stmt) : Stmt()
    data class Return(val expr: Expr?) : Stmt()
    data class Wrap(val expr: Expr) : Stmt()
}

sealed class Expr {
    object This : Expr()
    object Null : Expr()
    data class Var(val name: String) : Expr()
    data class Access(val lhs: Expr, val rhs: String) : Expr()
    data class Literal(val type: TypeRep.RPrimitive, val value: Any) : Expr()

    data class MethodCall(val typeArgs: Boolean = false, val lhs: Expr, val args: List<Expr>) : Expr()
    data class ConstructorCall(val typeArgs: Boolean = false, val lhs: Expr, val args: List<Expr>) : Expr()

    data class Binary(val lhs: Expr, val op: Op, val rhs: Expr) : Expr() {
        enum class Op { `==`, `!=` }
    }

    data class Unary(val op: Op, val expr: Expr) : Expr() {
        enum class Op { `!` }
    }

    fun wrap(): Stmt = Stmt.Wrap(this)

    infix fun `=`(rhs: Expr) = Assign(this, rhs)

    operator fun get(memberName: String): Expr = Access(this, memberName)

}


enum class Modifier {
    Public, Final, Private
}