/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

@file:Suppress("ObjectPropertyName")

package syb4j.types

import syb4j.types.TypeRep.RObj
import syb4j.types.TypeRep.RObj.Companion.`java'lang'Object`
import syb4j.types.TypeRep.RPrimitive.Type
import syb4j.types.TypeRep.RTypeVar
import arrow.optics.Traversal
import arrow.optics.optics

/**
 * Java class, annotated with [adt.Sealed]
 * */
/*@optics*/ data class SealedClass<AST>(
    val name: QualifiedName,
    val typeVars: List<RTypeVar>,
    val cases: List<CaseClass<AST>>,
    val astElement: AST,
) {
    companion object
}


/**
 * Java class, annotated with [adt.Case]
 * */
/*@optics*/ data class CaseClass<AST>(
    val name: QualifiedName,
    val typeVars: List<RTypeVar>,
    val extendsClause: RObj?,
    val fields: List<Field>,
    val astElement: AST,
) {
    fun isSubclassCandidate(sealedClass: SealedClass<AST>): Boolean =
        extendsClause == null
                || extendsClause == `java'lang'Object`
                || extendsClause.name == sealedClass.name

    companion object
}

/**
 * Field in a case class
 * */
@optics data class Field(
    val name: String,
    val type: TypeRep,
) {
    companion object
}


/**
 * type representations
 * */
@optics sealed class TypeRep {

    /**
     * Primitive types
     * */
    @optics data class RPrimitive(val type: Type) : TypeRep() {
        enum class Type {
            Int, Bool, Char, Float, Double, Byte, Unit
        }

        companion object {
            operator fun invoke(string: String): TypeRep = when (string) {
                "int" -> RPrimitive(Type.Int)
                "boolean" -> RPrimitive(Type.Bool)
                "char" -> RPrimitive(Type.Char)
                "float" -> RPrimitive(Type.Float)
                "double" -> RPrimitive(Type.Double)
                "byte" -> RPrimitive(Type.Byte)
                "void" -> RPrimitive(Type.Unit)
                else -> RObj(QualifiedName(string), emptyList())
            }
        }
    }

    /**
     * Any other type
     * */
    @optics
    data class RObj(
        val name: QualifiedName,
        val typeVars: List<TypeRep>,
    ) : TypeRep() {


        companion object {
            val `java'lang'Object` = RObj(QualifiedName(java.lang.Object::class.java.canonicalName), emptyList())
            fun RObj(name: String, typeVars: List<TypeRep>) =
                RObj(QualifiedName(name), typeVars)
        }
    }

    /**
     * type variables
     * */
    @optics data class RTypeVar(
        val name: String,
        val isExistential: Boolean = false,
        /*    val bounds: TypeBounds = TypeBounds.None,*/
    ) : TypeRep() {
        companion object
    }

    object RWildcard : TypeRep()

    internal val nestedTypeVars: List<RTypeVar>
        get() = when (this) {
            is RTypeVar -> listOf(this)
            is RObj -> typeVars.flatMap(TypeRep::nestedTypeVars)
            else -> emptyList()
        }

    fun toJavaString(): StringBuilder = when (this) {
        RWildcard -> StringBuilder("?")
        is RTypeVar -> StringBuilder(name)
        is RObj -> StringBuilder(name.name).apply {
            if (typeVars.isNotEmpty()) {
                typeVars.joinTo(
                    this,
                    separator = ", ",
                    prefix = "<",
                    postfix = ">"
                ) { it.toJavaString() }
            }
        }
        is RPrimitive -> when (type) {
            Type.Int -> StringBuilder("int")
            Type.Bool -> StringBuilder("boolean")
            Type.Char -> StringBuilder("char")
            Type.Float -> StringBuilder("float")
            Type.Double -> StringBuilder("double")
            Type.Byte -> StringBuilder("byte")
            Type.Unit -> StringBuilder("void")
        }
    }


    companion object {
        private val typeVarTraversal = (RObj.typeVars + Traversal.list())
        internal fun modifyTypeVars(f: (RTypeVar) -> RTypeVar): (TypeRep) -> TypeRep =
            { type ->
                when (type) {
                    is RTypeVar -> f(type)
                    is RObj -> typeVarTraversal.modify(type, modifyTypeVars(f))
                    else -> type
                }
            }
    }

}

/**
 * Smart constructor to ensure all type names are fully qualified
 * */
@JvmInline
value class QualifiedName(val name: String) {
    val simpleName: String
        get() = name.substringAfterLast('.')

    companion object {
        operator fun invoke(name: CharSequence) = QualifiedName(name.toString())
    }
}


/*
@optics sealed interface TypeBounds {
    object None : TypeBounds

    @optics data class Eq(val to: TypeRep) : TypeBounds {
        companion object
    }

    companion object
}
*/


//object Unit : TypeRep
//object Bool : TypeRep
//object Num : TypeRep
//object String : TypeRep
