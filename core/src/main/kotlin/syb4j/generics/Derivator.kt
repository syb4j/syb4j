/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

package syb4j.generics

import syb4j.types.ADT
import syb4j.types.Constructor
import syb4j.types.Method

interface Derivator {
    fun deriveDataMethods(adt: ADT): List<Method> = emptyList()
    fun deriveConstructorMethods(case: Constructor): List<Method> = emptyList()
}