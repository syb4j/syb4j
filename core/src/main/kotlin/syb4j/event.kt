package syb4j

import java.util.*

sealed class Event<A>(val eventData: A) {
    abstract val timestamp: Long
    abstract val eventId: UUID
}

data class BemerungUpdated(
    override val timestamp: Long,
    override val eventId: UUID,
    val newBemerkung: String
): Event<String>(newBemerkung)

data class InterneNummerUpdated(
    override val timestamp: Long,
    override val eventId: UUID,
    val nummer: Int
): Event<Int>(nummer)


fun <A> f(event: Event<A>){
    when(event){
        is BemerungUpdated -> TODO()
        is InterneNummerUpdated -> TODO()
    }
}