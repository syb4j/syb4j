package syb4j

import org.jetbrains.annotations.NonNls
import org.jetbrains.annotations.PropertyKey
import java.text.MessageFormat
import java.util.*

@NonNls
private const val BUNDLE = "messages.CoreBundle"

object CoreBundle {

    private val resources by lazy { ResourceBundle.getBundle(BUNDLE) }

    @Suppress("SpreadOperator")
    @JvmStatic
    fun message(@PropertyKey(resourceBundle = BUNDLE) id: String, vararg args: Any?): String =
        try {
            MessageFormat.format(resources.getString(id), *args)
        } catch (e: IllegalArgumentException) { //if formatting is incorrect
            e.localizedMessage
        }

}
