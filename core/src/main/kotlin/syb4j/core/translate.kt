/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

package syb4j.core

import syb4j.generics.Derivator
import syb4j.guard
import syb4j.modify
import syb4j.types.*
import syb4j.types.TypeRep.RObj.Companion.`java'lang'Object`
import syb4j.types.TypeRep.RTypeVar
import arrow.core.Either
import arrow.core.computations.either
import arrow.core.traverseEither


internal fun <AST> translate(
    caseClass: CaseClass<AST>,
    sealedClass: SealedClass<AST>,
): Try<AST, Pair<CaseClass<AST>, Constructor>> =
    either.eager<ADTError, Constructor> {

        guard(!caseClass.isSubclassCandidate(sealedClass)) { ADTError.WRONG_EXTENDS_CLAUSE }
        val typeVarBindings = caseClass.bindTypeVarsToParent(sealedClass)
        val explicitTVars = caseClass.resolveExplicitTypeVars(typeVarBindings)

        // Resolve name clashes
        val fields = caseClass.fields.map(Field.type.modify(TypeRep.modifyTypeVars { v -> explicitTVars[v] ?: v }))
        val resolvedTypeVarBindings =
            typeVarBindings.mapValues { (_, type) -> TypeRep.modifyTypeVars { v -> explicitTVars[v] ?: v }(type) }

        // used type vars in fields and extends clause
        val usedTypeVars = fields.flatMapTo(hashSetOf()) { it.type.nestedTypeVars }
            .plus(resolvedTypeVarBindings.flatMapTo(hashSetOf()) { (_, type) -> type.nestedTypeVars })

        // If any of the parents type var is bound to a concrete type
        val isGADT = resolvedTypeVarBindings.any { (_, type) -> type !is RTypeVar }

        guard(isGADT && usedTypeVars.any { !explicitTVars.containsValue(it) }) {
            ADTError.GADT_MIXED_IMPLICIT_EXPLICIT
        }

        val actualTypeVars = (explicitTVars.values + usedTypeVars)
            .resolveExistentialTypeVars(explicitTVars, resolvedTypeVarBindings)

        Constructor(
            caseClass.name,
            actualTypeVars,
            resolvedTypeVarBindings.filter { (_, binding) -> binding !is RTypeVar },
            fields
        )

    }.bimap({ caseClass.astElement to it }, { caseClass to it })


private fun <AST> CaseClass<AST>.bindTypeVarsToParent(sealedClass: SealedClass<AST>): Map<String, TypeRep> {
    val caseTypeVars =
        if (extendsClause == null || extendsClause == `java'lang'Object`) sealedClass.typeVars
        else extendsClause.typeVars

    return sealedClass.typeVars.zip(caseTypeVars)
        .associate { (dataTVar, binding) -> dataTVar.name to binding }
}

private fun List<RTypeVar>.resolveExistentialTypeVars(
    explicitTVars: Map<RTypeVar, RTypeVar>,
    actualParentBindings: Map<String, TypeRep>,
): List<RTypeVar> = mapTo(hashSetOf()) {
    val isUniversal =
        explicitTVars.containsKey(it) && actualParentBindings.containsValue(it) || //its universal if its passed directly to the parent type
                !explicitTVars.containsKey(it) && actualParentBindings.containsKey(it.name) //or its implicit
    RTypeVar.isExistential.set(it, !isUniversal)
}.toList()


private fun <AST> CaseClass<AST>.resolveExplicitTypeVars(parentTVarBindings: Map<String, TypeRep>): Map<RTypeVar, RTypeVar> {
    /*

 fun genTypeVarName(): Sequence<String> {
    return generateSequence(0, Int::inc)
        .map(::to26System andThen StringBuilder::toString)
}

 fun to26System(i: Int): StringBuilder {
    val iToC = ('A'.code + (i % 26)).toChar()
    return if (i < 26) {
        StringBuilder().append(iToC)
    } else {
        to26System(i / 26 - 1).append(iToC)
    }
}
    val indexed = typeVars.mapTo(hashSetOf()) { it.name }
    val nameSeq = genTypeVarName()
        .filter { !indexed.contains(it) && !parentTVarBindings.containsKey(it) }
        .iterator()
    return typeVars.associateWith {
        if (parentTVarBindings.containsKey(it.name) && parentTVarBindings[it.name] != it)
            it //RTypeVar(nameSeq.next()) // Replace name clashes
        else it
    }*/
    return typeVars.associateWith { it }
}

