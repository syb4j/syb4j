/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

@file:Suppress("LocalVariableName", "ObjectPropertyName")

package syb4j.core

import syb4j.Case
import syb4j.Sealed
import syb4j.generics.Derivator
import syb4j.types.ADT
import syb4j.types.ADTOut
import syb4j.types.QualifiedName
import syb4j.types.SealedClass
import arrow.core.Either
import arrow.core.traverseEither


internal typealias Try<AST, ADT> = Either<Pair<AST, ADTError>, ADT>
typealias SybResult<AST> = Try<AST, ADTOut<AST>>

val `@Sealed` = QualifiedName(Sealed::class.java.canonicalName)
val `@Case` = QualifiedName(Case::class.java.canonicalName)

fun <AST> process(sealedClass: SealedClass<AST>, derivators: List<Derivator>): SybResult<AST> =
    sealedClass.cases
        .traverseEither { caseAST -> translate(caseAST, sealedClass) }
        .map { cs ->
            ADT(sealedClass.name, sealedClass.typeVars, cs.map { it.second })
                .generate(sealedClass, cs, derivators)
        }

