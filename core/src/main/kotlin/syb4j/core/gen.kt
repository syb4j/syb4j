/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

@file:Suppress("LocalVariableName", "ObjectPropertyName")

package syb4j.core

import syb4j.generics.Derivator
import syb4j.types.*
import syb4j.types.Expr.*
import syb4j.types.Modifier.Private
import syb4j.types.Modifier.Public
import syb4j.types.Stmt.Block
import syb4j.types.Stmt.Return

private val javaFun1 = QualifiedName(java.util.function.Function::class.java.canonicalName)

//private val javaFun2 = QualifiedName(java.util.function.BiFunction::class.java.canonicalName)
private val `@Override` = QualifiedName(Override::class.java.canonicalName)

private const val invokeFun = "apply"

internal fun <AST> ADT.generate(
    sealedClass: SealedClass<AST>,
    cs: List<Pair<CaseClass<AST>, Constructor>>,
    derivators: List<Derivator>
) =
    ADTOut(
        ast = sealedClass.astElement,
        adt = this,
        constructors = cs.map { (astElem, con) -> ConstructorOut(astElem.astElement, con, genCaseMethods(con, derivators)) },
        methods = genDataMethods(derivators)
    )


private fun ADT.genDataMethods(derivators: List<Derivator>): List<Method> {
    val staticConstructors = constructors.map { con ->
        Method.Normal(
            isStatic = true,
            name = con.name.simpleName,
            modifiers = setOf(Public),
            typeParams = con.typeVars,
            params = con.fields,
            returnType = typeRep(con.boundTypeVars),
            code = Block(
                listOf(
                    Return(
                        ConstructorCall(
                            typeArgs = con.typeVars.isNotEmpty(),
                            Var(this.name.simpleName)[con.name.simpleName],
                            con.fields.map { Var(it.name) })
                    )
                )
            )
        )
    }
    val privateConstructor = Method.Constructor(modifiers = setOf(Private), name = name.simpleName)

    val R = TypeRep.RTypeVar("R")
    val abstractMatcher = Method.Abstract(
        name = "match",
        modifiers = setOf(Public),
        typeParams = listOf(R),
        params = constructors.map { con ->
            Field(
                con.name.simpleName.lowercase(),
                TypeRep.RObj(javaFun1, listOf(con.typeRepFromParent, R))
            )
        },
        returnType = R
    )

    val derived = derivators.flatMap { it.deriveDataMethods(this) }

    return staticConstructors +
            privateConstructor +
            abstractMatcher +
            derived


}

private fun ADT.genCaseMethods(constructor: Constructor, derivators: List<Derivator>): List<Method> {
    val privateConstructor =
        Method.Constructor(modifiers = setOf(Private),
            params = constructor.fields,
            assignFields = true,
            name = constructor.name.simpleName)

    val R = TypeRep.RTypeVar("R")
    val concreteMatcher = Method.Normal(
        annotations = listOf(`@Override`),
        isStatic = false,
        name = "match",
        modifiers = setOf(Public),
        typeParams = listOf(R),
        params = constructors.map { con ->
            Field(
                con.name.simpleName.lowercase(),
                TypeRep.RObj(javaFun1, listOf(con.typeRepFromCase(constructor), R))
            )
        },
        returnType = R,
        code = Block(
            listOf(
                Return(MethodCall(typeArgs = false,
                    Var(constructor.name.simpleName.lowercase())[invokeFun],
                    listOf(This)))
            )
        )
    )
    val derived = derivators.flatMap { it.deriveConstructorMethods(constructor) }

    return listOf(privateConstructor, concreteMatcher) + derived
}