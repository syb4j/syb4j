/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

package syb4j.core;

import syb4j.CoreBundle.message

enum class ADTError {
    GADT_MIXED_IMPLICIT_EXPLICIT,
    WRONG_EXTENDS_CLAUSE;

    val pretty: String
        get() = when(this){
            GADT_MIXED_IMPLICIT_EXPLICIT -> message("error.gadt-mixed-implicit-explicit")
            WRONG_EXTENDS_CLAUSE -> message("error.wrong-extends-clause")
        }
}