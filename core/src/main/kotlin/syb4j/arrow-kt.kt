@file:Suppress("NOTHING_TO_INLINE")

package syb4j

import arrow.core.Either.Left
import arrow.core.Option
import arrow.core.computations.RestrictedEitherEffect
import arrow.optics.Optional
import arrow.optics.dsl.at
import arrow.optics.dsl.index
import arrow.optics.typeclasses.At
import arrow.optics.typeclasses.Index


internal suspend fun <E, A> RestrictedEitherEffect<E, A>.guard(errorCondition: Boolean, error: () -> E) {
    if (errorCondition) Left(error()).bind<A>()
}

internal fun <S, A> Optional<S, A>.modify(f: (A) -> A): (S) -> S = { s -> modify(s, f) }
internal fun <S, A> Optional<S, A>.set(focus: A): (S) -> S = { set(it, focus) }
internal inline fun <S, A> Optional<S, A>.update(crossinline f: (S) -> A): (S) -> S = { set(it, f(it)) }
internal fun <T, K, V> Optional<T, Map<K, V>>.atIndex(key: K): Optional<T, V> = index(Index.map(), key)
internal fun <T, K, V> Optional<T, Map<K, V>>.at(key: K): Optional<T, Option<V>> = at(At.map(), key)

internal inline infix fun <A, B, C> ((A) -> B).andThen(crossinline f: (B) -> C): (A) -> C = { a -> f(invoke(a)) }

internal inline fun <A> id(a: A): A = a

//λ ¬ ∀
