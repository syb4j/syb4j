package generics;

import syb4j.generics.Derivator;
import syb4j.types.*;
import syb4j.types.Expr.Access;
import syb4j.types.Expr.MethodCall;
import syb4j.types.Expr.Var;
import syb4j.types.Stmt.Block;
import syb4j.types.Stmt.Return;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Set;

public class ToStringDerivator implements Derivator {
    @NotNull @Override
    public List<Method> deriveDataMethods(@NotNull ADT adt) {
        return List.of();
    }

    @NotNull @Override public List<Method> deriveConstructorMethods(@NotNull Constructor constructor) {
        return List.of(
            new Method.Normal(
                List.of(/*new QualifiedName("Override")*/),
                false,
                "toString2",
                Set.of(Modifier.Public),
                List.of(),
                List.of(),
                TypeRep.RObj.Companion.RObj("String", List.of()),
                new Block(List.of(
                    new Return(
                        new MethodCall(false,
                            new Access(
                                new MethodCall(false, new Var("getClass"), List.of()),
                                "getName"),
                            List.of()
                        )
                    )
                )
                ))
        );
    }
}
