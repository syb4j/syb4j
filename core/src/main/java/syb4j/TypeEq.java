/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

package syb4j;

public abstract class TypeEq<A, B> {
    private TypeEq() {
    }

    public abstract B coerce(A a);

    public abstract A subst(B b);

    private static final TypeEq<Object, Object> refl = new TypeEq<Object, Object>() {

        @Override public Object coerce(Object o) {
            return o;
        }

        @Override public Object subst(Object o) {
            return o;
        }
    };

    @SuppressWarnings("unchecked")
    public static <T> TypeEq<T, T> refl() {
        return (TypeEq<T, T>) refl;
    }


}