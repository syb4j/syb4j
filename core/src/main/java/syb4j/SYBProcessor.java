/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

package syb4j;
/*

import com.sun.source.util.Trees;
import com.sun.tools.javac.code.Flags;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Set;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
@interface Sealed2 {}

@SupportedAnnotationTypes("Sealed")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class SYBProcessor extends AbstractProcessor {
    Trees trees;
    TreeMaker make;
    Name.Table names;
    Symtab symtab;

    @Override
    public void init(ProcessingEnvironment env) {
        Context context = ((JavacProcessingEnvironment) env).getContext();
        trees = Trees.instance(processingEnv);
        make = TreeMaker.instance(context);
        names = Names.instance(context).table;
        symtab = Symtab.instance(context);
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (Element each : roundEnv.getElementsAnnotatedWith(Sealed.class)) {
            if (each.getKind() == ElementKind.CLASS) {
                JCTree.JCClassDecl tree = (JCTree.JCClassDecl) trees.getTree(each);
                Name rName = names.fromString("R");
                JCTree matchMethod = make.MethodDef(
                    make.Modifiers(Flags.PUBLIC | Flags.ABSTRACT),
                    names.fromString("match"),                           // Methodenname
                    make.Ident(rName),                                      // Returntyp
                    List.of(make.TypeParameter(rName, List.nil())),         // Typparameter
                    List.nil(),                                             // Argumente
                    List.nil(),                                             // Throws-Liste
                    null,                                              // Methodenkörper
                    null                                          // Defaultwert für Interfaces
                );
                tree.defs.append(matchMethod);
            }
        }
        return false;
    }
}*/
