package helper

import syb4j.javac.JavacProcessor
import java.io.File
import java.nio.charset.StandardCharsets
import java.util.*
import javax.annotation.processing.AbstractProcessor
import javax.tools.Diagnostic
import javax.tools.DiagnosticCollector
import javax.tools.JavaFileObject
import javax.tools.ToolProvider

data class Compilation(
    val outputFiles: List<JavaFileObject>,
    val diagnostic: List<Diagnostic<*>>,
    val succeeded: Boolean
)

fun withCompiledSources(
    fullyQualifiedName: String,
    source: String,
    writeToStdOut: Boolean = true,
    f: Compilation.() -> Unit
) = f(compile(SourceFileObject(fullyQualifiedName, source), writeToStdOut))

fun withCompiledResources(
    path: String,
    writeToStdOut: Boolean = true,
    f: Compilation.() -> Unit
) = f(compile(ResourceFileObject(File(path).toURI()), writeToStdOut))


private fun compile(
    fileObj: JavaFileObject,
    debug: Boolean,
    options: List<String> = listOf("-printsource" /*"--module-source-path", "test-src/src"*//*"-source", "1.8"*/),
    processors: List<AbstractProcessor> = listOf(JavacProcessor())
): Compilation {
    val compiler = ToolProvider.getSystemJavaCompiler()
//    EclipseCompiler()
//org.eclipse.jdt.internal.compiler.Compiler()
//    CompilerOptions().apply {
//        processAnnotations = true
//        generateClassFiles = false
//    }
    val diagnosticCollector = DiagnosticCollector<JavaFileObject>()
    val fileManager = FileManager(
        compiler.getStandardFileManager(
            diagnosticCollector,
            Locale.getDefault(),
            StandardCharsets.UTF_8
        )
    )

    // For classpath
    // fileManager.setLocation(StandardLocation.CLASS_PATH, classPath)

    val out = if (debug) System.err.bufferedWriter() else null
    val files = listOf(fileObj)

    val task = compiler.getTask(out, fileManager, diagnosticCollector, options, emptyList(), files)
        .apply { setProcessors(processors)}

    val succeeded = task.call()
    if (debug) {
        diagnosticCollector.diagnostics.forEach {
            when (it.kind) {
                Diagnostic.Kind.ERROR -> System.err.println(it)
                else -> println(it)
            }
        }
    }
    return Compilation(fileManager.outputFiles, diagnosticCollector.diagnostics, succeeded)
}
