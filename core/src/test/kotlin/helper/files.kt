package helper

import java.io.*
import java.net.URI
import java.nio.charset.Charset
import java.util.concurrent.ConcurrentHashMap
import javax.tools.*

internal class FileManager(private val manager: StandardJavaFileManager) :
    ForwardingJavaFileManager<StandardJavaFileManager>(manager), StandardJavaFileManager {

    private val outputCache = ConcurrentHashMap<URI, JavaFileObject>()

    val outputFiles: List<JavaFileObject>
        get() = outputCache.values.toList()

    private fun getOrCreate(key: URI) =
        outputCache.computeIfAbsent(key) { InMemoryFileObject(it) }

    private fun createURI(location: JavaFileManager.Location, packageName: String, relativeName: String): URI {
        val packageURL =
            if (packageName.isEmpty()) ""
            else packageName.replace('.', '/') + "/"
        return URI.create("mem:///${location.name}/$packageURL$relativeName")
    }

    private fun createURI(location: JavaFileManager.Location, className: String, kind: JavaFileObject.Kind): URI =
        URI.create("mem:///${location.name}/${className.replace('.', '/')}${kind.extension}")

    override fun getJavaFileForInput(
        location: JavaFileManager.Location,
        className: String,
        kind: JavaFileObject.Kind
    ): JavaFileObject? =
        if (location.isOutputLocation) {
            outputCache[createURI(location, className, kind)]
        } else {
            super.getJavaFileForInput(location, className, kind)
        }

    override fun getFileForInput(
        location: JavaFileManager.Location,
        packageName: String,
        relativeName: String
    ): FileObject? =
        if (location.isOutputLocation) {
            outputCache[createURI(location, packageName, relativeName)]
        } else {
            super.getFileForInput(location, packageName, relativeName)
        }

    override fun getJavaFileForOutput(
        location: JavaFileManager.Location,
        className: String,
        kind: JavaFileObject.Kind,
        sibling: FileObject?
    ): JavaFileObject = getOrCreate(createURI(location, className, kind))

    override fun getFileForOutput(
        location: JavaFileManager.Location,
        packageName: String,
        relativeName: String,
        sibling: FileObject?
    ): FileObject = getOrCreate(createURI(location, packageName, relativeName))

    override fun getJavaFileObjectsFromFiles(files: MutableIterable<File>?): MutableIterable<JavaFileObject> =
        manager.getJavaFileObjectsFromFiles(files)

    override fun getJavaFileObjects(vararg files: File?): MutableIterable<JavaFileObject> =
        manager.getJavaFileObjects(*files)

    override fun getJavaFileObjects(vararg names: String?): MutableIterable<JavaFileObject> =
        manager.getJavaFileObjects(*names)

    override fun getJavaFileObjectsFromStrings(names: MutableIterable<String>?): MutableIterable<JavaFileObject> =
        manager.getJavaFileObjectsFromStrings(names)

    override fun setLocation(location: JavaFileManager.Location?, files: MutableIterable<File>?) =
        manager.setLocation(location, files)

    override fun getLocation(location: JavaFileManager.Location): MutableIterable<File> =
         manager.getLocation(location) ?: mutableListOf(File.createTempFile(location.name, "class"))
}


internal class InMemoryFileObject(uri: URI, val charset: Charset = Charsets.UTF_8) :
    SimpleJavaFileObject(uri, uri.getKind()) {
    private var data: ByteArray? = null

    override fun openInputStream(): InputStream =
        data?.inputStream() ?: throw FileNotFoundException()

    override fun openOutputStream(): OutputStream = object : ByteArrayOutputStream() {
        override fun close() {
            super.close()
            data = toByteArray()
        }
    }

    override fun openReader(ignoreEncodingErrors: Boolean): Reader =
        openInputStream().reader(charset)

    override fun getCharContent(ignoreEncodingErrors: Boolean): CharSequence =
        data?.toString(charset) ?: throw FileNotFoundException()

    override fun openWriter(): Writer = object : StringWriter() {
        override fun close() {
            super.close()
            data = toString().toByteArray(charset)
        }
    }

    companion object {

        fun URI.getKind(): JavaFileObject.Kind =
            JavaFileObject.Kind.values()
                .find { path.endsWith(it.extension) } ?: JavaFileObject.Kind.OTHER
    }
}

