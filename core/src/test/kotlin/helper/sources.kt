package helper

import helper.InMemoryFileObject.Companion.getKind
import java.io.InputStream
import java.io.Reader
import java.io.StringReader
import java.net.URI
import javax.tools.JavaFileObject
import javax.tools.SimpleJavaFileObject

internal class SourceFileObject(fullyQualifiedName: String, private val source: String) :
    SimpleJavaFileObject(
        URI.create(fullyQualifiedName.replace('.', '/') + JavaFileObject.Kind.SOURCE.extension),
        JavaFileObject.Kind.SOURCE
    ) {

    override fun openInputStream(): InputStream = source.byteInputStream(Charsets.UTF_8)

    override fun openReader(ignoreEncodingErrors: Boolean): Reader = StringReader(source)

    override fun getCharContent(ignoreEncodingErrors: Boolean): CharSequence = source
}

internal class ResourceFileObject(uri: URI) : SimpleJavaFileObject(uri, uri.getKind()) {

    override fun openInputStream(): InputStream =
        toUri().toURL().openStream()

    override fun openReader(ignoreEncodingErrors: Boolean): Reader =
        openInputStream().reader(Charsets.UTF_8)

    override fun getCharContent(ignoreEncodingErrors: Boolean): CharSequence =
        openReader(ignoreEncodingErrors).use { it.readText() }
}