package syb4j

import helper.withCompiledResources
import kotlin.test.Test


internal class ExprTest {

    @Test
    fun testSimpleADT() = withCompiledResources("test-src/src/Expr.java") {
        outputFiles.forEach { println(it.getCharContent(true)) }
    }
}