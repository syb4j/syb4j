/*
 * Copyright (c) 2021, syb4j.
 * All rights reserved.
 */

package syb4j

import helper.withCompiledResources
import helper.withCompiledSources
import javax.tools.Diagnostic
import kotlin.test.Test


//language=java
const val SOURCE = """
            import syb4j.*;
    
            @Sealed
            public class Test {
                public static void main(String[] args) {
                    assert "ASD" == "asd" : "Test";
                }
            }
            @Sealed
            class Test2 {
                @Case static class Test2a {}
                public static void main(String[] args) {
                    assert "ASD" == "asd" : "Test";
                }
            }
        """

internal class SourceTest {

    @Test
    fun test() = withCompiledSources("Test", SOURCE.trimIndent()) {
        diagnostic.forEach {
            when (it.kind) {
                Diagnostic.Kind.ERROR -> error(it)
                else -> println(it)
            }
        }
        outputFiles.forEach { print(it.getCharContent(true)) }
    }

    @Test
    fun test2() = withCompiledResources("test-src/src/SimpleADT.java") {

        outputFiles.forEach { print(it.getCharContent(true)) }
    }
}