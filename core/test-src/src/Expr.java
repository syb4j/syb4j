import syb4j.Case;
import syb4j.Sealed;

@Sealed class Expr<A> {

    @Case static class Lit<A> {
        A a;
    }

    @Case static class Add extends Expr<Integer> {
        Expr<Integer> a;
        Expr<Integer> b;
    }

    @Case static class Eq<B> extends Expr<Boolean> {
        Expr<B> a;
        Expr<B> b;
    }

    void asd(java.util.function.Function<Integer, String> f){}
}

